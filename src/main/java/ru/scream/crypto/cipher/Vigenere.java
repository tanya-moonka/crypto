package ru.scream.crypto.cipher;

import ru.scream.crypto.base.Cipher;

import java.io.BufferedReader;
import java.io.BufferedWriter;

/**
 * Шифр Вижинера
 *
 * Метод полиалфавитного шифрования буквенного текста с использованием ключевого слова.
 */
public class Vigenere extends Cipher
{
    @Override
    public boolean validateKey()
    {
        boolean valid = false;
        if (this.key.trim() != "") {
            valid = true;
        }

        return valid;
    }


    @Override
    public void encrypt(BufferedReader reader, BufferedWriter writer)
    {

    }

    @Override
    public void decrypt(BufferedReader reader, BufferedWriter writer)
    {}

    @Override
    public void hack(BufferedReader reader, BufferedWriter writer)
    {}
}
